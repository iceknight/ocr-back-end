package kg.iceknight.ocrbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OcrBackEndApplication {

    public static void main(String[] args) {
        SpringApplication.run(OcrBackEndApplication.class, args);
    }
}

