package kg.iceknight.ocrbackend.controller;

import kg.iceknight.ocrbackend.properties.GlobalProperties;
import kg.iceknight.ocrbackend.service.ModelEvaluater;
import kg.iceknight.ocrbackend.service.NetworkFactory;
import kg.iceknight.ocrbackend.service.NetworkTrainer;
import org.datavec.api.io.filters.BalancedPathFilter;
import org.datavec.api.io.labels.ParentPathLabelGenerator;
import org.datavec.api.split.FileSplit;
import org.datavec.api.split.InputSplit;
import org.datavec.image.loader.BaseImageLoader;
import org.datavec.image.recordreader.ImageRecordReader;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.linalg.dataset.api.preprocessor.DataNormalization;
import org.nd4j.linalg.dataset.api.preprocessor.ImagePreProcessingScaler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.IOException;
import java.util.Random;

@RestController
@RequestMapping("/api/ocr")
public class OCRController {

    private final GlobalProperties globalProperties;
    private final NetworkTrainer networkTrainer;
    private final NetworkFactory networkFactory;
    private final ModelEvaluater modelEvaluater;

    @Autowired
    public OCRController(GlobalProperties globalProperties,
                         NetworkTrainer networkTrainer,
                         NetworkFactory networkFactory,
                         ModelEvaluater modelEvaluater) {
        this.globalProperties = globalProperties;
        this.networkTrainer = networkTrainer;
        this.networkFactory = networkFactory;
        this.modelEvaluater = modelEvaluater;
    }

    @RequestMapping(value = "/train", method = RequestMethod.GET)
    public void get() throws IOException {
        File parentDir = new File(globalProperties.getImageDir());
        FileSplit filesInDir = new FileSplit(parentDir,
                BaseImageLoader.ALLOWED_FORMATS,
                new Random(12345));
        ParentPathLabelGenerator labelMaker = new ParentPathLabelGenerator();
        BalancedPathFilter pathFilter = new BalancedPathFilter(new Random(12345),
                BaseImageLoader.ALLOWED_FORMATS,
                labelMaker);

        InputSplit[] filesInDirSplit = filesInDir.sample(pathFilter, 80, 20);
        InputSplit trainData = filesInDirSplit[0];
        InputSplit testData = filesInDirSplit[1];

        ImageRecordReader recordReader = new ImageRecordReader(globalProperties.getHeight(),
                globalProperties.getWidth(),
                globalProperties.getChannels(),
                labelMaker);

        recordReader.initialize(testData, null);

        DataNormalization scaler = new ImagePreProcessingScaler(0, 1);

        MultiLayerNetwork network = networkFactory.alexnetModel();
        networkTrainer.trainModel(trainData, recordReader, scaler, network);


        Evaluation eval = modelEvaluater.evaluateModel(testData, recordReader, scaler, network);

        System.out.println(eval.stats(true));

        String basePath = System.getProperty("user.dir") + "/src/main/resources/";

        File file = new File(basePath + "model.bin");
        if (!file.exists()) {
            file.createNewFile();
        }

        ModelSerializer.writeModel(network, basePath + "model.bin", true);

    }
}
