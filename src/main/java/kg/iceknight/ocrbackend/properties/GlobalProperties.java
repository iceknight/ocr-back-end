package kg.iceknight.ocrbackend.properties;

import lombok.Data;
import org.datavec.image.loader.BaseImageLoader;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.Random;

@Data
@Component
@ConfigurationProperties(prefix = "ocr")
public class GlobalProperties {
    private int batchSize;
    private int iterations;
    private int epochs;
    private String[] allowedExtensions = BaseImageLoader.ALLOWED_FORMATS;
    private long seed;
    private Random randNumGen = new Random(12454);
    private int height;
    private int width;
    private int channels;
    private int outputNum;
    private String dataset;
    private File baseDir = new File(System.getProperty("user.dir"));
    private String imageDir = baseDir + File.separator + "dataset";
}
