package kg.iceknight.ocrbackend.service;

import org.datavec.api.split.InputSplit;
import org.datavec.image.recordreader.ImageRecordReader;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.dataset.api.preprocessor.DataNormalization;

import java.io.IOException;

public interface ModelEvaluater {
    Evaluation evaluateModel(InputSplit testData,
                             ImageRecordReader recordReader,
                             DataNormalization scaler, MultiLayerNetwork network) throws IOException;
}
