package kg.iceknight.ocrbackend.service;

import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;

public interface NetworkFactory {
    MultiLayerNetwork alexnetModel();
}
