package kg.iceknight.ocrbackend.service;

import org.datavec.api.split.InputSplit;
import org.datavec.image.recordreader.ImageRecordReader;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.dataset.api.preprocessor.DataNormalization;

public interface NetworkTrainer {
    void trainModel(InputSplit trainData,
                    ImageRecordReader recordReader,
                    DataNormalization scaler,
                    MultiLayerNetwork network);
}
