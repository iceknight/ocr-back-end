package kg.iceknight.ocrbackend.service;

import java.io.InputStream;

public interface OCRService {
    public String recognize(InputStream imgStream);
}
