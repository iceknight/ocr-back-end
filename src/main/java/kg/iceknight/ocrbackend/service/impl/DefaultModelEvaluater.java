package kg.iceknight.ocrbackend.service.impl;

import kg.iceknight.ocrbackend.properties.GlobalProperties;
import kg.iceknight.ocrbackend.service.ModelEvaluater;
import org.datavec.api.split.InputSplit;
import org.datavec.image.recordreader.ImageRecordReader;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.api.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.dataset.api.preprocessor.DataNormalization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class DefaultModelEvaluater implements ModelEvaluater {

    private final GlobalProperties globalProperties;

    @Autowired
    public DefaultModelEvaluater(GlobalProperties globalProperties) {
        this.globalProperties = globalProperties;
    }

    public Evaluation evaluateModel(InputSplit testData,
                                    ImageRecordReader recordReader,
                                    DataNormalization scaler, MultiLayerNetwork network) throws IOException {
        recordReader.reset();
        recordReader.initialize(testData, null);
        DataSetIterator testIter = new RecordReaderDataSetIterator(recordReader,
                globalProperties.getBatchSize(),
                1,
                globalProperties.getOutputNum());
        scaler.fit(testIter);
        testIter.setPreProcessor(scaler);
        Evaluation eval = network.evaluate(testIter);

        while (testIter.hasNext()) {
            DataSet next = testIter.next();
            INDArray output = network.output(next.getFeatureMatrix());
            eval.eval(next.getLabels(), output);
        }

        return eval;

    }

}
