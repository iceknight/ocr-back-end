package kg.iceknight.ocrbackend.service.impl;

import kg.iceknight.ocrbackend.properties.GlobalProperties;
import kg.iceknight.ocrbackend.service.NetworkTrainer;
import org.datavec.api.split.InputSplit;
import org.datavec.image.recordreader.ImageRecordReader;
import org.datavec.image.transform.FlipImageTransform;
import org.datavec.image.transform.ImageTransform;
import org.datavec.image.transform.WarpImageTransform;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.deeplearning4j.datasets.iterator.MultipleEpochsIterator;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.dataset.api.preprocessor.DataNormalization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

@Service
public class DefaultNetworkTrainer implements NetworkTrainer {

    private final GlobalProperties globalProperties;

    @Autowired
    public DefaultNetworkTrainer(GlobalProperties globalProperties) {
        this.globalProperties = globalProperties;
    }

    public void trainModel(InputSplit trainData,
                            ImageRecordReader recordReader,
                            DataNormalization scaler,
                            MultiLayerNetwork network) {
        ImageTransform flipTransform1 = new FlipImageTransform(globalProperties.getRandNumGen());
        ImageTransform flipTransform2 = new FlipImageTransform(new Random(globalProperties.getSeed() * 3));
        ImageTransform warpTransform = new WarpImageTransform(globalProperties.getRandNumGen(), 42);
        List<ImageTransform> transforms = Arrays.asList(new ImageTransform[]{flipTransform1, warpTransform, flipTransform2});

        final DataSetIterator[] dataIter = new DataSetIterator[1];

        transforms.forEach(transform -> {
            System.out.print("\nTraining on transformation: " + transform.getClass().toString() + "\n\n");
            try {
                recordReader.initialize(trainData, transform);
            } catch (IOException e) {
                e.printStackTrace();
            }
            dataIter[0] = new RecordReaderDataSetIterator(recordReader, globalProperties.getBatchSize(),
                    1, globalProperties.getOutputNum());
            scaler.fit(dataIter[0]);
            dataIter[0].setPreProcessor(scaler);
            MultipleEpochsIterator trainIter = new MultipleEpochsIterator(globalProperties.getEpochs(), dataIter[0]);
            network.fit(trainIter);
        });
    }

}
